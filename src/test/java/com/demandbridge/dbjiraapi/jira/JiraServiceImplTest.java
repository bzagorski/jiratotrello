package com.demandbridge.dbjiraapi.jira;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.concurrent.TimeUnit;

class JiraServiceImplTest {

    @Test
    void testErrorHandling() {
        var intFlux = Flux.just(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                .concatWith(Flux.error(new RuntimeException(">> Error Occurred <<")))
//                .concatWith(Flux.just(11, 12, 13, 14, 15))
                .doOnError(e -> {
                    try {
                        Thread.sleep(TimeUnit.SECONDS.toMillis(10));
                    } catch (InterruptedException interruptedException) {
                        interruptedException.printStackTrace();
                    }
                })
                .onErrorResume(e -> {
                    System.out.println(e.getMessage());
                    return Flux.just(100);
                });

        StepVerifier.create(intFlux.log())
                .expectSubscription()
                .expectNext(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                .expectNext(100)
                .verifyComplete();
    }

    @Test
    public void fluxErrorHandling_withRetry() {

        Flux<String> stringFlux = Flux.just("a", "b", "c")
                .concatWith(Flux.error(new RuntimeException("Exception Occurred")))
                .concatWith(Flux.just("D"))

                .onErrorMap(CustomException::new)
                .retry(2);

        // P.s. Retry produces same stream again

        StepVerifier.create(stringFlux.log())
                .expectSubscription()
                .expectNext("a", "b", "c")
                .expectNext("a", "b", "c")
                .expectNext("a", "b", "c")
                .expectError(CustomException.class)
                .verify();
    }

    protected static class CustomException extends Throwable {

        public CustomException(Throwable e) {
            super(e);
        }
    }

}