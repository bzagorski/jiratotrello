package com.demandbridge.dbjiraapi.ui;

import com.demandbridge.dbjiraapi.jira.JiraService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
public class IndexController {

    private final JiraService jiraService;

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("greeting", "Hello World");
        model.addAttribute("activeSprint", jiraService.getActiveSprint());
        model.addAttribute("tickets", jiraService.getTicketsInActiveSprint());
        return "index";
    }
}
