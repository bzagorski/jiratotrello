package com.demandbridge.dbjiraapi.ui;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/mockup")
public class MockupController {

    @GetMapping
    public String mockup() {
        return "mockup";
    }
}
