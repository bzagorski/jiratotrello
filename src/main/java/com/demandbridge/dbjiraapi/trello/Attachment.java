package com.demandbridge.dbjiraapi.trello;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Attachment {

    private String id;

    private String key;

    private String token;

    private String url;
}

