package com.demandbridge.dbjiraapi.trello;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Label {

    private String id;

    private String idBoard;

    private String name;

    private String color;
}
