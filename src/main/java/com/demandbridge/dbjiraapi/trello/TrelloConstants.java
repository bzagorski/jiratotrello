package com.demandbridge.dbjiraapi.trello;

public class TrelloConstants {

    public enum Lists {
        RELEASE("RELEASE_ID");

        private String id;

        Lists(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }
    }

    public enum Boards {
        RELEASE_NIGHT("RELEASE_LIST_ID");

        private String id;

        Boards(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }
    }

    public enum Labels {
        JIRA_TICKET("JIRA_TICKET_LABEL_ID"),
        COMMERCE("COMMERCE_TICKET_LABEL_ID"),
        SOURCING("SOURCING_TICKET_LABEL_ID"),
        PAY("PAY_TICKET_LABEL_ID"),
        CAMPAIGN_MANAGER("CAMPAIGN_MANAGER_LABEL_ID");

        private String id;

        Labels(String id) {
            this.id = id;
        }

        public String getId() {
            return this.id;
        }
    }

}
