package com.demandbridge.dbjiraapi.trello;

import com.fasterxml.jackson.databind.JsonNode;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Set;

public interface TrelloClient {
    Flux<Card> getCardsOnBoard(String boardId);

    Mono<Card> addCard(Card card, String listId);

    Flux<Label> getLabels();

    Mono<Label> createLabel(Label label, String boardId);

    Mono<JsonNode> addLabelToCard(Label label, String cardId);

    Mono<Attachment> addAttachmentToCard(Attachment attachment, String cardId);

    Mono<Void> archiveAllCardsInList(String listId);
}
