package com.demandbridge.dbjiraapi.trello;

import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Builder
@ToString(exclude = {"description", "labels", "attachments"})
@EqualsAndHashCode(of = {"id", "name"})
@NoArgsConstructor
@AllArgsConstructor
public class Card {

    @Builder.Default
    private String key = "";

    @Builder.Default
    private String id = "";

    @Builder.Default
    private String name = "";

    @Builder.Default
    private String description = "";

    @Builder.Default
    private String idList = "";

    @Builder.Default
    private String url = "";

    @Builder.Default
    private Set<Label> labels = new HashSet<>();

    @Builder.Default
    private Set<Attachment> attachments = new HashSet<>();
}
