package com.demandbridge.dbjiraapi.trello;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
public class TrelloController {

    private final TrelloClient trelloClient;

    @GetMapping("/trello/releaseBoard/cards")
    public Flux<Card> releaseNightBoardCards() {
        return trelloClient.getCardsOnBoard(TrelloConstants.Boards.RELEASE_NIGHT.getId());
    }

    @GetMapping("/trello/releaseBoard/labels")
    public Flux<Label> getLabels() {
        return trelloClient.getLabels();
    }

    @PostMapping("/trello/releaseBoard/labels")
    public Mono<Label> createLabel(@RequestBody Label label) {
        return trelloClient.createLabel(label, TrelloConstants.Boards.RELEASE_NIGHT.getId());
    }

    @PostMapping("/trello/releaseBoard/cards/{cardId}/labels")
    public Mono<JsonNode> addLabelToCard(@PathVariable String cardId, @RequestBody Label label) {
        return trelloClient.addLabelToCard(label, cardId);
    }

    @PostMapping("/trello/releaseBoard/cards")
    public Mono<Card> createCard(@RequestBody Card card) {
        return trelloClient.addCard(card, TrelloConstants.Lists.RELEASE.getId());
    }

    @PostMapping("/trello/list/{listId}/archiveAllCards")
    public Mono<Void> archiveAllCards(@PathVariable String listId) {
        return trelloClient.archiveAllCardsInList(listId);
    }
}
