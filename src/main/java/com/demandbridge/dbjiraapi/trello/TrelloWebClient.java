package com.demandbridge.dbjiraapi.trello;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class TrelloWebClient implements TrelloClient {

    @Value("${trello.key}")
    private String key;

    @Value("${trello.token}")
    private String token;

    private final WebClient trelloClient = WebClient.builder()
            .baseUrl("https://api.trello.com/1")
            .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
            .build();

    @Override
    public Flux<Card> getCardsOnBoard(String boardId) {
        return trelloClient.get()
                .uri(uriBuilder -> uriBuilder.path("/boards/" + boardId + "/cards")
                        .queryParam("token", token)
                        .queryParam("key", key)
                        .queryParam("labels", "all")
                        .queryParam("label_fields", "id", "color", "name")
                        .build())
                .retrieve()
                .bodyToFlux(Card.class);
    }

    @Override
    public Mono<Card> addCard(Card card, String listId) {
        card.setIdList(listId);
        return trelloClient.post()
                .uri(buildUri("/cards"))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .body(BodyInserters.fromFormData("idList", card.getIdList())
                        .with("name", card.getName())
                        .with("desc", card.getDescription())
                        .with("idLabels", card.getLabels().stream().map(Label::getId).collect(Collectors.joining(",")))
                )
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse ->
                        Mono.just(new RuntimeException(String.format(
                                "Error adding card: %s\nHeaders received: %s", card.getKey(), clientResponse.headers().toString()))))
                .bodyToMono(Card.class)
                .log("card-added");
    }

    @Override
    public Flux<Label> getLabels() {
        return trelloClient.get()
                .uri(buildUri("/boards/" + TrelloConstants.Boards.RELEASE_NIGHT.getId() + "/labels"))
                .retrieve()
                .bodyToFlux(Label.class);
    }

    @Override
    public Mono<Label> createLabel(Label label, String boardId) {
        label.setIdBoard(boardId);
        return trelloClient.post()
                .uri(buildUri("/labels"))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .body(BodyInserters.fromFormData(
                        "name", label.getName())
                        .with("color", label.getColor())
                        .with("idBoard", label.getIdBoard()))
                .retrieve()
                .bodyToMono(Label.class);
    }

    @Override
    public Mono<JsonNode> addLabelToCard(Label label, String cardId) {
        return trelloClient.post()
                .uri(buildUri("/cards/" + cardId + "/idLabels"))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .body(BodyInserters.fromFormData("value", label.getId()))
                .retrieve()
                .bodyToMono(JsonNode.class)
                .log("added-label");
    }

    @Override
    public Mono<Void> archiveAllCardsInList(String listId) {
        return trelloClient.post()
                .uri(buildUri("/lists/" + listId + "/archiveAllCards"))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .retrieve()
                .bodyToMono(Void.class);
    }

    @Override
    public Mono<Attachment> addAttachmentToCard(Attachment attachment, String cardId) {
        return trelloClient.post()
                .uri(buildUri("/cards/" + cardId + "/attachments"))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .body(BodyInserters.fromFormData("url", attachment.getUrl()))
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse ->
                        Mono.just(new RuntimeException(String.format(
                                "Error adding attachment: %s\nHeaders received: %s", attachment.getUrl(), clientResponse.headers().toString()))))
                .bodyToMono(Attachment.class)
                .log("attachment-added");
    }

    private Function<UriBuilder, URI> buildUri(String uri) {
        return uriBuilder -> uriBuilder.path(uri)
                .queryParam("token", token)
                .queryParam("key", key)
                .build();
    }
}
