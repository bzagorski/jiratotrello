package com.demandbridge.dbjiraapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class JiraToTrelloApplication {

    public static void main(String[] args) {
        SpringApplication.run(JiraToTrelloApplication.class, args);
    }

}
