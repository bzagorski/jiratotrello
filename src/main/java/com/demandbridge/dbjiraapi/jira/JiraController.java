package com.demandbridge.dbjiraapi.jira;

import com.demandbridge.dbjiraapi.trello.Attachment;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/jira")
public class JiraController {

    private final JiraService jiraService;

    @GetMapping("/sprints/active")
    public Mono<Sprint> getActiveSprint() {
        return jiraService.getActiveSprint();
    }

    @GetMapping("/sprints/{sprintId}/tickets")
    public Mono<JiraResponse> getTicketsInSprint(@PathVariable int sprintId) {
        return jiraService.getTicketsInSprint(sprintId);
    }

    @GetMapping("/sprints/active/tickets")
    public Mono<JiraResponse> getTicketsInActiveSprint() {
        return jiraService.getActiveSprintAndTickets();
    }

    @PostMapping("/sprints/active/initializeTrelloBoard")
    public Flux<Attachment> initializeReleaseBoard() {
        return jiraService.initializeReleaseBoard();
    }

    @GetMapping("/tickets/{ticketNumber}")
    public Mono<JsonNode> getJiraTicket(@PathVariable String ticketNumber) {
        return jiraService.getJiraTicket(ticketNumber);
    }
}
