package com.demandbridge.dbjiraapi.jira;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Sprint {

    private int id;

    private String self;

    private String state;

    private String name;
}
