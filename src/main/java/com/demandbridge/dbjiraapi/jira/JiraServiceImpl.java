package com.demandbridge.dbjiraapi.jira;

import com.demandbridge.dbjiraapi.trello.*;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class JiraServiceImpl implements JiraService {

    private final JiraClient jiraClient;
    private final TrelloClient trelloClient;

    @Override
    public Mono<Sprint> getActiveSprint() {
        return jiraClient.getActiveSprint()
                .log(">> Getting active sprint <<");
    }

    @Override
    public Mono<JiraResponse> getActiveSprintAndTickets() {
        return jiraClient.getTicketsInActiveSprint()
                .onErrorReturn(JiraResponse.builder().build())
                .log();
    }

    @Override
    @Cacheable("tickets")
    public Flux<JiraTicket> getTicketsInActiveSprint() {
        return jiraClient.getTicketsInActiveSprint()
                .flatMapMany(jiraResponse -> Flux.fromIterable(jiraResponse.getIssues()))
                .sort(Comparator.comparing(JiraTicket::getKey))
                .log(">> Getting tickets from active sprint <<");
    }

    @Override
    public Mono<JiraResponse> getTicketsInSprint(Integer sprintId) {
        return jiraClient.getTicketsInSprint(sprintId).log(">> Getting active sprint <<");
    }

    @Override
    public Mono<JsonNode> getJiraTicket(String ticketNumber) {
        return jiraClient.getJiraTicket(ticketNumber);
    }

    @Override
    public Flux<Attachment> initializeReleaseBoard() {
        Mono<JiraResponse> release = jiraClient.getTicketsInActiveSprint()
                .log("jira-retrieved-active-sprint");

        Mono<Label> sprintLabel = jiraClient.getActiveSprint()
                .flatMap(sprint -> trelloClient.createLabel(
                        Label.builder().name(sprint.getName()).color("black").build(), TrelloConstants.Boards.RELEASE_NIGHT.getId()))
                .log("trello-created-release-label");

        return Flux.zip(release, sprintLabel)
                .flatMap(sprintStuff -> {
                    var releaseResponse = sprintStuff.getT1();
                    var releaseLabel = sprintStuff.getT2();
                    var tickets = releaseResponse.getIssues();
                    tickets.sort(Comparator.comparing(JiraTicket::getKey));

                    return Flux.fromStream(tickets.stream().map(jiraTicket -> {
                                Set<Label> labels = new HashSet<>();
                                if (jiraTicket.getKey().contains("DBE")) {
                                    labels.add(Label.builder().id(TrelloConstants.Labels.COMMERCE.getId()).build());
                                } else if (jiraTicket.getKey().contains("DBC")) {
                                    labels.add(Label.builder().id(TrelloConstants.Labels.SOURCING.getId()).build());
                                } else if (jiraTicket.getKey().contains("PAY")) {
                                    labels.add(Label.builder().id(TrelloConstants.Labels.PAY.getId()).build());
                                } else if (jiraTicket.getKey().contains("CM")) {
                                    labels.add(Label.builder().id(TrelloConstants.Labels.CAMPAIGN_MANAGER.getId()).build());
                                }
                                labels.add(Label.builder().id(TrelloConstants.Labels.JIRA_TICKET.getId()).build());
                                labels.add(releaseLabel);

                                return Card.builder()
                                        .key(jiraTicket.getKey())
                                        .name(String.format("%s - %s", jiraTicket.getKey(), jiraTicket.getFields().getOrDefault("summary", "")))
                                        .url(jiraTicket.getSelf())
                                        .description(jiraTicket.getFields().getOrDefault("description", ""))
                                        .labels(labels)
                                        .build();
                            }
                    ))
                            .delayElements(Duration.ofSeconds(1))
                            .log(">> Adding card <<")
                            .flatMap(card -> trelloClient.addCard(card, TrelloConstants.Lists.RELEASE.getId())
                                    .delayElement(Duration.ofSeconds(3))
                                    .flatMap(createdCard -> {
                                        createdCard.setUrl(String.format("https://service.demandbridge.com/browse/%s", card.getKey()));
                                        createdCard.setLabels(card.getLabels());
                                        return Mono.just(createdCard);
                                    }))
                            .flatMap(card -> trelloClient.addAttachmentToCard(
                                    Attachment.builder().url(card.getUrl()).build(), card.getId()
                            ))
                            .log(">> Attachment added <<");
                });
    }
}
