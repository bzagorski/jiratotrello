package com.demandbridge.dbjiraapi.jira;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JiraResponse {

    @Builder.Default
    private int total = 0;

    @Builder.Default
    private int maxResults = 0;

    @Builder.Default
    private List<JiraTicket> issues = new ArrayList<>();
}
