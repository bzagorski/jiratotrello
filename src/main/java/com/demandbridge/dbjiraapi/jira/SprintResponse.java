package com.demandbridge.dbjiraapi.jira;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SprintResponse {

    private int total;

    private boolean isLast;

    @JsonAlias("values")
    private List<Sprint> sprints = new ArrayList<>();
}
