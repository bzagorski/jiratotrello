package com.demandbridge.dbjiraapi.jira;

import com.demandbridge.dbjiraapi.trello.Attachment;
import com.fasterxml.jackson.databind.JsonNode;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface JiraService {

    Mono<Sprint> getActiveSprint();

    Mono<JiraResponse> getActiveSprintAndTickets();

    Flux<JiraTicket> getTicketsInActiveSprint();

    Mono<JiraResponse> getTicketsInSprint(Integer sprintId);

    Mono<JsonNode> getJiraTicket(String ticketNumber);

    Flux<Attachment> initializeReleaseBoard();

}
