package com.demandbridge.dbjiraapi.jira;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;

@Service
public class JiraClient {

    // Improvement: id: 4
    // type: "Functionality Bug Task" id: 31
    // sprint custom field:

    @Value("${jira.token}")
    private String token;

    private WebClient jiraClient;

    @PostConstruct
    private void initClient() {
        jiraClient = WebClient.builder()
                .baseUrl("https://service.demandbridge.com/rest")
                .defaultHeaders(httpHeaders -> httpHeaders.setBearerAuth(token))
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
                .build();
    }

    public Mono<Sprint> getActiveSprint() {
        return jiraClient.get()
                .uri("/agile/1.0/board/10/sprint?state=ACTIVE")
                .retrieve()
                .bodyToMono(SprintResponse.class)
                .flatMap(sprintResponse -> Mono.just(sprintResponse.getSprints().get(0)));
    }

    public Flux<JsonNode> searchForSprints(String query) {
        return jiraClient.get()
                .uri(uriBuilder -> uriBuilder.path("/api/2/jql/autocompletedata/suggestions")
                        .queryParam("fieldName", "Sprint")
                        .queryParam("fieldValue", query).build())
                .retrieve()
                .bodyToFlux(JsonNode.class);
    }

    public Mono<JiraResponse> getTicketsInSprint(int sprint) {
        return jiraClient.get()
                .uri(uriBuilder -> uriBuilder.path("/api/latest/search")
                        .queryParam("jql", "sprint=" + sprint)
                        .queryParam("fields", "id", "key", "summary", "description")
                        .queryParam("maxResults", 1000)
                        .build())
                .retrieve()
                .bodyToMono(JiraResponse.class);
    }

    public Mono<JiraResponse> getTicketsInActiveSprint() {
        return jiraClient.get()
                .uri(uriBuilder -> uriBuilder.path("/api/latest/search")
                        .queryParam("jql", "Sprint in openSprints() and project in (\"DB/Enterprise\", \"DB Alliance\", \"Pay Application\", \"Campaign Manager\")")
                        .queryParam("fields", "id", "key", "summary", "description")
                        .queryParam("maxResults", 1000)
                        .build())
                .retrieve()
                .bodyToMono(JiraResponse.class);
    }

    public Mono<JsonNode> getJiraTicket(String ticketNumber) {
        return jiraClient.get()
                .uri("/api/latest/issue/" + ticketNumber)
                .retrieve()
                .bodyToMono(JsonNode.class);
    }
}
