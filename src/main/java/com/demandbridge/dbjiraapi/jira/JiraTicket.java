package com.demandbridge.dbjiraapi.jira;

import lombok.*;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"key", "self"})
public class JiraTicket {

    private int id;

    @Builder.Default
    private String key = "";

    @Builder.Default
    private String summary = "";

    @Builder.Default
    private String self = "";

    @Builder.Default
    private String url = "";

    @Builder.Default
    Map<String, String> fields = new HashMap<>();

    public String getUrl() {
        return String.format("https://service.demandbridge.com/browse/%s", this.key);
    }

    public String getPlatform() {
        if (this.key.contains("DBE")) {
            return "commerce";
        } else if (this.key.contains("DBC")) {
            return "sourcing";
        } else if (this.key.contains("PAY")) {
            return "payapp";
        } else if (this.key.contains("CM")) {
            return "campaign_manager";
        } else {
            return "";
        }
    }
}
