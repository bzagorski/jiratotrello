console.log('loaded')

document.getElementById('initButton').addEventListener('click', function(e) {
    e.preventDefault();
    if (confirm('Confirm that you are ready to add all the Jira tickets listed here to the Trello Release Night board. This action cannot be undone.')) {
        fetch('/api/v1/jira/sprints/active/initializeTrelloBoard', { method: 'POST'})
            .then(response => response.json())
            .then(data => {
                console.log('board successfully initialized', data)
                alert('Release tickets successfully added to trello board')
            })
            .catch(error => {
                console.log('Error occurred while adding tickets to trello board', error)
                alert('An error occurred while adding tickets to the release night board. Please contact bzagorski@demandbridge.com')
            })
    }
})