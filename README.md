# DemandBridge Jira Trello Api Utility

### Usage

#### REST:
* GET - active sprint: "/api/v1/jira/sprints/active"
* GET - tickets in active sprint "/api/v1/jira/sprints/active/tickets"
* GET - ticket in a sprint: "/api/v1/jira/sprints/{sprintId}/tickets"
* GET - Add current sprint tickets to Release Night trello board Release list: "/api/v1/jira/sprints/active/initializeTrelloBoard"

### Todo - API
* ~~Parse Jira tickets into objects~~
* ~~Create trello card objects from jira tickets~~
* ~~Update trello cards with release night board id~~
* ~~Update trello cards with correct labels~~
* ~~Create label for current sprint~~
* ~~Get Current Spring metadata~~
* ~~Search for sprints~~
* ~~Get Tickets for selected sprint~~
* Create Tests
* Create 3 week scheduler
* Create front end ui

### Todo - UI
* ~~Select current sprint by default~~ - Just going to get the current active sprint for Hunt Valley
* ~~Allow searching for sprint~~ ^
* Ticket list populates with sprint tickets and allows for checking/unchecking
* Submit button to initialize the board
* Button to clear the Release list